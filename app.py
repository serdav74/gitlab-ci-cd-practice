import unittest


def sum(a: int, b: int) -> int:
    return a + b


class MyTest(unittest.TestCase):
    def test(self):
        self.assertEqual(sum(2, 2), 4)



